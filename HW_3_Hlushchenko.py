# Сформировать строку, в которой содержится информация определенном слове в строке. Например "The [номер] symbol in
# [тут слово] is [значение символа по номеру в слове]". Слово и номер получите с помощью input() или воспользуйтесь
# константой. Например (если слово - "Python" а символ 3) - "The 3 symbol in "Python" is t".

while True:
    word = input('Write a word:  ')
    if not word.isalpha():
        print("Do not use special symbols or digits for word")
        continue
    break

while True:
    try:
        number = int(input('Write a whole number:  '))
    except:
        print("Not a whole number! Please try again")
        continue

    if number < 0:
        print("Please write a positive number!")
    elif number == 0:
        print("Please do not use zero!")
    elif number > len(word):
        print(f"Word is shorter than {number} symbols")
    else:
        break

my_str = f'The {number}th symbol in "{word}" is \"{word[number - 1]}\"'
print(my_str)


# _______________________________________________________________________________________________________

# Ввести из консоли строку. Определить количество слов в этой строке, которые заканчиваются на букву "o"
# (учтите, что буквы бывают заглавными).

my_str = input("Your message: ")
my_list = my_str.lower().split()

target_list = []
counter = 0
for word in my_list:
    if word[-1] == "o" or word[-1] == "о":       # кирилица и латиница
        target_list.append(word)
        counter += 1
print(target_list)
print(f" String has {counter} words with \"o\" letter at the end")

# -------------------------------------------------------------------------------------------------------------------

# Есть list с данными lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишите механизм, который cформирует новый list (например lst2), который содержит только переменные типа str,
# которые есть в lst1.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []
for element in lst1:
    if type(element) == str:
        lst2.append(element)
print(lst2)