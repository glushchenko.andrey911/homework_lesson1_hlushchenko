def yes_or_no():
    """
    Function for converting user answer to False or True values.
    :return: bool
    """
    user_answer = input("Do you want to continue")

    while user_answer not in ("Y", "N"):
        user_answer = input('Please use just "Y"(yes) or "N"(No) options to answer.')

    if user_answer == "Y":
        return True
    elif user_answer == "N":
        return False

