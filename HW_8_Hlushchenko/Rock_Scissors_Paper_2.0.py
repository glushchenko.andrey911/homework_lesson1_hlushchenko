from lib import user_choice, computer_choice, results_to_file


def is_user_win(rules_to_win, user_figure, computer_figure):
    """
    Function compares user & computer figures according to the rules in dictionary. In case figures are the same it
    returns None, if value (comp.figure) matching key (user figure) function returns True. In other cases returns False.
    Args:
        rules_to_win (dict):
        user_figure (str):
        computer_figure (str):
    Returns:
        (bool|None)
    """
    if user_figure == computer_figure:
        return

    if computer_figure in rules_to_win[user_figure]:
        return True

    return False


def make_message(result, user_figure, computer_figure):
    """
    Function returns massage based on result of the game and figures that have both computer and player.
    Args:
        result (bool|None):
        user_figure (str):
        computer_figure (str):
    Returns:
        str
    """
    dct = {
        True: 'User win',
        False: 'Computer win',
        None: 'Draw!',
    }

    msg = f'User - {user_figure}, computer - {computer_figure}, result is - {dct[result]} \n'

    return msg


def rspls_game():
    """
    Function is used for playing advanced Rock, Scissors, Paper game. Results are printed and saved to the file after
    playing
    Returns:
        str
    """
    rules_to_win = {
        'Rock': ['Scissors', 'Lizard'],
        'Scissors': ['Paper', 'Lizard'],
        'Paper': ['Rock', 'Spock'],
        'Lizard': ['Spock', 'Paper'],
        'Spock': ['Scissors', 'Rock'],
    }

    user_figure = user_choice(*rules_to_win.keys())

    computer_figure = computer_choice(*rules_to_win.keys())

    game_result = is_user_win(rules_to_win, user_figure, computer_figure)

    message = make_message(game_result, user_figure, computer_figure)

    results_to_file(message)
    return message


print(rspls_game())





