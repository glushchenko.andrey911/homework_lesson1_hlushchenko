class Vehicle:
    """ Class contains initialisation method to define base attributes that are common for all types of vehicles. """
    is_moving = False

    def __init__(self, max_speed, mass, passengers_capacity, color):
        self.max_speed = max_speed
        self.mass = mass
        self.passengers_capacity = passengers_capacity
        self.color = color

    def start_moving(self):
        self.is_moving = True

    def stop_moving(self):
        self.is_moving = False

    def current_state(self):
        if self.is_moving is True:
            print("Vehicle is moving")
        else:
            print("Vehicle is not moving")


class Car(Vehicle):

    def __init__(self, max_speed, mass, passengers_capacity, color, autobody_type):
        Vehicle.__init__(self, max_speed, mass, passengers_capacity, color)
        self.autobody_type = autobody_type


volvo256 = Car(200, 1500, 5, "Red", "Hatchback")
print(volvo256.mass)
volvo256.start_moving()
volvo256.current_state()