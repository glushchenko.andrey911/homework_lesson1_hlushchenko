# Доработайте классы Point и Line из занятия. Обеспечьте передачу в first_point и second_point класса Line только
# обьектов класса Point с помощью property

from time import process_time


class Point:
    """ Class to define point by coordinats"""

    def __init__(self, coord_x, coord_y):
        self.x = coord_x
        self.y = coord_y

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    @x.setter
    def x(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError("Value type must be a numeric")
        self.__x = value

    @y.setter
    def y(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError("Value type must be a numeric")
        self.__y = value

    def __str__(self):
        return f'point with coordinates: ({self.x}, {self.y}) '

    def __add__(self, other):
        return Line(self, other)


class Line:
    """ Class to initialize line object creation and calculation its properties based on inputted points"""

    def __init__(self, start_point, end_point):
        self.start_point = start_point
        self.end_point = end_point

    @property
    def start_point(self):
        return self.__start_point

    @start_point.setter
    def start_point(self, value):
        if not isinstance(value, Point):
            raise TypeError("Value type must be a Point")
        self.__start_point = value

    @property
    def end_point(self):
        return self.__end_point

    @end_point.setter
    def end_point(self, value):
        if not isinstance(value, Point):
            raise TypeError("Value type must be a Point")
        self.__end_point = value

    @property
    def length(self):
        """
        Method to calculate line length based on two points
        Returns:
             float: line length
        """

        leg_x = (self.start_point.x - self.end_point.x)
        leg_y = (self.start_point.y - self.end_point.y)
        hypotenuse = (leg_x ** 2 + leg_y ** 2) ** 0.5
        return round(hypotenuse, 3)

    def __str__(self):
        return f'Line from {self.start_point} to {self.end_point} with length of {self.length}'


# Напишите декоратор, который замеряет и принтует время выполнения функции

def time_tracker(function_to_decorate):
    """ Decorator to calculate elapsed function run time """
    def wrapper(*args, **kwargs):

        start_time = process_time()
        res = function_to_decorate(*args, **kwargs)
        end_time = round((process_time() - start_time), 4)
        print(f'Time elapsed: {end_time}')
        return res

    return wrapper

# function usage example

# @time_tracker
# def strange_factorial(n):
#
#     def solving(arg):
#         if arg == 0:
#             return 1
#         return ((solving(arg-1)*arg) + (solving(arg-1)*arg))/2
#
#     return solving(n)
#
#
# print(strange_factorial(25))

