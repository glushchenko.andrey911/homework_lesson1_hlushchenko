# Напишіть функцію (хто може - клас) яка приймає довільний текст і знаходить в ньому всі номерні знаки України і
# повертає їх у вигляді списку.
#
# Стандарти номерних знаків
#
# АB1234CD
# 12 345-67 AB
# a12345BC

import re


class FindLicPlate:
    """Class to initialize text analysis based on predefined patterns (regular expressions)"""

    def __init__(self, text: str):
        results = []
        pattern_1 = '[A-Z]{2}\d{4}[A-Z]{2}'
        pattern_2 = '\d{2} \d{3}\-\d{2} [A-Z]{2}'
        pattern_3 = '[a-z]\d{5}[A-Z]{2}'
        rules = (pattern_1,
                 pattern_2,
                 pattern_3)

        for rule in rules:
            match = re.findall(rule, text)
            results.extend(match)

        self.results = results

    def print_results(self):
        """ Method for printing analysis results"""

        print(self.results)

# simple example for QA

# a = FindLicPlate("AB1234CD, DD1234FG 12 345-67 AB, sdergdf4 3323tf 56 6sdg, 55 354-55 YH, a12345BC, i12345FF")
# a.print_results()

