# Создайте класс "Транспортное средство" и отнаследуйтесь от него классами "Самолет", "Автомобиль", "Корабль".
# Продумайте и реализуйте в классе "Транспортное средство" общие атрибуты для "Самолет", "Автомобиль", "Корабль".
# В наследниках реализуйте характерные для них атрибуты и методы

class Vehicle:
    """Class to define base vehicle parameters & state"""

    in_motion = "is not moving"

    def __init__(self,
                 max_speed: int = None,
                 mass: int = None,
                 passenger_capacity: int = None,
                 color: str = None):

        self.max_speed = max_speed
        self.mass = mass
        self.passenger_capacity = passenger_capacity
        self.color = color

    def run(self):
        """
        Changes the state of the vehicle to "in motion"
        """
        self.in_motion = "is in motion"

    def brake(self):
        """
        Changes the state of the vehicle to "not moving"
        """
        self.in_motion = "is not moving"


class Car(Vehicle):
    """Class to define additional parameters that are common for cars.
    Based on general vehicle parameters & state"""

    def __init__(self,
                 max_speed,
                 mass,
                 passenger_capacity,
                 color,
                 doors_qty: int = None):

        Vehicle.__init__(self,
                         max_speed,
                         mass,
                         passenger_capacity,
                         color)

        self.doors_qty = doors_qty

    def current_state(self):
        """ Creates a massage to reflect current state of a car and its specification"""

        print(f"This {self.color} car with mass of {self.mass} kg {self.in_motion}."
              f"It has {self.passenger_capacity} places to sit & {self.doors_qty} doors in total."
              f"This car may be accelerated up to {self.max_speed} km/h")


class Plane(Vehicle):
    """Class to define additional parameters that are common for planes.
    Based on general vehicle parameters & state"""

    def __init__(self,
                 max_speed,
                 mass,
                 passenger_capacity,
                 color,
                 wing_span: int = None):

        Vehicle.__init__(self,
                         max_speed,
                         mass,
                         passenger_capacity,
                         color)

        self.wing_span = wing_span

    def current_state(self):
        """ Creates a massage to reflect current state of a plane and its specification"""

        print(f"This {self.color} plane with mass of {self.mass} kg & {self.wing_span}m wing span {self.in_motion}."
              f"It has {self.passenger_capacity} places to sit in total"
              f"This plane may be accelerated up to {self.max_speed} km/h")


class Ship(Vehicle):
    """Class to define additional parameters that are common for ships.
    Based on general vehicle parameters & state"""

    def __init__(self,
                 max_speed,
                 mass,
                 passenger_capacity,
                 color,
                 draught: int = None):

        Vehicle.__init__(self,
                         max_speed,
                         mass,
                         passenger_capacity,
                         color)

        self.draught = draught

    def current_state(self):
        """ Creates a massage to reflect current state of a ship and its specification"""

        print(f"This {self.color} ship with mass of {self.mass}kg & draught of {self.draught}cm {self.in_motion}."
              f"It has {self.passenger_capacity} places to sit"
              f"This ship may be accelerated up to {self.max_speed} km/h")

# testing

# ford_fiesta = Car(max_speed=250, mass=1500, passenger_capacity=5, color="red", doors_qty=4)
# ford_fiesta.current_state()
#
# z28 = Plane(max_speed=1600, mass=12000, passenger_capacity=2, color="green", wing_span=20)
# z28.current_state()
#
# ataman = Ship(max_speed=75, mass=100000, passenger_capacity=300, color="black", draught=20)
# ataman.current_state()
