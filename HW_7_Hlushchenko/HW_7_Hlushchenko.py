# Пишем игру. Программа выбирает из диапазона чисел (пусть для начала будет 1-100) случайное число и предлагает
# пользователю его угадать. Пользователь вводит число. Если пользователь не угадал - предлагает пользователю угадать
# еще раз, пока он не угадает. Если угадал - спрашивает хочет ли он повторить игру (Y/N). Если Y - повторить. N -
# Прекратить. Опционально - добавьте в задание вывод сообщения-подсказки. Если пользователь ввел число, и не угадал -
# сообщать: "Холодно" если разница между загаданным и введенным числами больше 10, "Тепло" - если от 5 до 10 и "Горячо"
# если от 4 до 1.
import lib


def guess_the_number():
    """
    Playing simple game. Goal is to guess the number defined by machine.
    :return:
    """
    repeat = True
    while repeat:
        user_inp = input("Setup the range for the game: from 1 to ")
        while not user_inp.isnumeric():
            user_inp = input("Enter a positive number!")

        max_number = int(user_inp)
        if max_number > 5000:
            print("Looks like hard to guess :(")
            continue

        bot_number = lib.computer_number(max_number)

        is_winner = None
        while not is_winner:
            player_number = lib.user_number(max_number)
            is_winner = lib.smart_comparison(player_number, bot_number, max_number)

        while True:
            keep_play = input("One more time? :) (Y/N)")
            if keep_play == "Y":
                break
            elif keep_play == "N":
                repeat = False
                break


guess_the_number()
