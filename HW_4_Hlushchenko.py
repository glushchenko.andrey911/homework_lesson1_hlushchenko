#Есть строка произвольного содержания. Написать код, который найдет в строке самое короткое слово, в котором
# присутствуют подряд две гласные буквы.

# проверка введенной строки
while True:
    random_string = input("Enter something: ")
    if len(random_string) > 0 and len(set(random_string.split())) > 1:
        break
    else: print("Type minimum a pair of different words")

# чистка строки от лишних символов
counter = 0
for character in random_string:
    if not character.isalpha():
        random_string = random_string.replace(random_string[counter], ' ')
    counter += 1

list_from_string = random_string.lower().split()
vowels = set('aeiouyaуоиэыяюеё')         #латиница и кириллица

# составление списка слов с 2мя гласными
two_volwes_list = []
vowel_index = None

for word in list_from_string:
    for character in word:
        if character in vowels:
            if vowel_index is True:
                two_volwes_list.append(word)
            vowel_index = True
        else:
            vowel_index = False
    vowel_index = None

# Проверка на самое длинное слово
max_word = ''
if len(two_volwes_list) > 0:
    for word in two_volwes_list:
        if len(word) > len(max_word):
            max_word = word
    print(max_word)
else:
    print("No words with double vowel!")

#-----------------------------------------------------------------------------------------------------------
# Есть два числа - минимальная цена и максимальная цена. Дан словарь продавцов и цен на какой то товар у разных
# продавцов: { "citrus": 47.999, "istudio" 42.999, "moyo": 49.999, "royal-service": 37.245, "buy.ua": 38.324,
# "g-store": 37.166, "ipartner": 38.988, "sota": 37.720, "rozetka": 38.003}. Написать код, который найдет и выведет
# на экран список продавцов, чьи цены попадают в диапазон между нижней и верхней ценой. Например:


while True:
    try:
        low_limit = float (input("Type a lower price limit: "))
        upp_limit = float(input("Type a upper price limit: "))
        break
    except ValueError:
        print("Please input just numbers here!")

prices = {
    "citrus": 47.999,
    "istudio": 42.999,
    "moyo": 49.999,
    "royal-service": 37.245,
    "buy.ua": 38.324,
    "g-store": 37.166,
    "ipartner": 38.988,
    "sota": 37.720,
    "rozetka": 38.003
}

filtered_prices = []
for shop, price in prices.items():
    if (price >= low_limit) and (price <= upp_limit):
        filtered_prices.append(shop)

if len(filtered_prices) == 0:
    print("Please try different values")
else:
    print(f'Best shops for you: {filtered_prices}')

